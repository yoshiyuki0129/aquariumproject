$(function() {
  console.log('1');
  var now = new Date();
  var yyyymmdd = now.getFullYear() + ('0' + (now.getMonth() + 1)).slice(-2) + ('0' + now.getDate()).slice(-2);
  var csvfile = yyyymmdd + '.csv';
  $.get(csvfile, readCsv, 'text');
  console.log(csvfile);
});

//非同期処理(CSV,グラフ)
function readCsv(data) {
  var time; //時間
  var temperature; //水温
  var chartData = [];
  console.log('2');
  csv = $.csv.toArrays(data); //日付

  $(csv).each(function() {
    if (this.length > 0) {
      $(this).each(function(index) {
        if (index == 0) {
          time = this;
        } else {
          temperature = Number(this);
        }
      });
      var date = new Date('Thu, 1 May 2008 ' + time + ':00 +0900');
      chartData.push({
        date: date,
        visits: temperature
      });
    }
  });

  //折れ線グラフ設定
  am4core.ready(function() {
    // Themes begin
    am4core.useTheme(am4themes_dark);
    am4core.useTheme(am4themes_animated);
    // Themes end

    // Create chart
    var chart = am4core.create('chartdiv', am4charts.XYChart);
    chart.paddingRight = 10;
    console.log(chartData);
    chart.data = chartData;
    console.log('5');

    var dateAxis = chart.xAxes.push(new am4charts.DateAxis());
    dateAxis.baseInterval = {
      timeUnit: 'minute',
      count: 1
    };
    dateAxis.tooltipDateFormat = 'HH:mm, d MMMM';

    var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
    valueAxis.tooltip.disabled = false;
    valueAxis.renderer.labels.template.disabled = true;

    var series = chart.series.push(new am4charts.LineSeries());
    series.dataFields.dateX = 'date';
    series.dataFields.valueY = 'visits';
    series.tooltipText = 'Visits: [bold]{valueY}[/]';
    series.fillOpacity = 0.3;

    chart.cursor = new am4charts.XYCursor();
    chart.cursor.lineY.opacity = 0;
    chart.scrollbarX = new am4charts.XYChartScrollbar();
    chart.scrollbarX.series.push(series);

    dateAxis.keepSelection = true;
  }); // end am4core.ready()
}
